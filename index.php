<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Evaluation 2</title>
    <link type="text/css" rel="stylesheet" href="./css/style.css">
</head>

<body>
    <header>
        <navbar class="container">
            <a href="index.php">
                <div class="btn_menu">Accueil</div>
            </a>
            <a href="presentation.html">
                <div class="btn_menu">Présentation du service</div>
            </a>
            <a href="produit.html">
                <div class="btn_menu">Liste des produits</div>
            </a>
            <a href="connexion.html">
                <div class="btn_menu">Connexion</div>
            </a>
            <a href="inscription.html">
                <div class="btn_menu">Inscription</div>
            </a>
           
        </navbar>
       
</header>
<section id="accueil">
        <div class="home">
            <br>
            <h1>Accueil</h1>
            <br>
            <a href="inscription.html"><div class="btn_inscription">S'inscrire</div></a>
            <br>
        <div>
    </section>
    <section class="res_pres">
      <h2>Présentation résumé du service</h2>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sed culpa ipsum alias, perspiciatis sequi pariatur id hic,
        <br> iste nemo dolore quibusdam, asperiores fugit eveniet rem ad vero. Similique, velit molestias!<p>
            <br>
            <a href="#"><div class="btn">Details</div></a>
    </section>
    <section class="produits" id="produits">
            <h2>Produits</h2>
            <div class="container_produits">
                <div class="prod"><img src="strawberries-1330459_640.jpg"><br>Produit 1</div>
                <div class="prod"><img src="bread-2193537_640.jpg"><br>Produit 2</div>
                <div class="prod"><img src="pretzel-2759994_640.jpg"><br>Produit 3</div>
                <div class="prod"><img src="walnut-1739021_640.jpg"><br>Produit 4</div>
            </div>
        <a href="#"><div class="btn inscription">S'inscrire</div></a>
    </section>

    <section class="avis_client">
        <h2>Témoignage</h2>
        <div class="container_cite">
            <blockquote>Exellent, <cite> Louis</cite></blockquote>
            <blockquote>Succulant, <cite> Alex</cite></blockquote>
            <blockquote>Ravisant, <cite> JB</cite></blockquote>
        </div>
    </section>
    <section id="contact" class="contact">
        <form method="post" action="./back/mail.php">
            <label>Pr&#xE9;nom *</label>
            <br/>
            <input name="firstname" type="text" value="" required="required" pattern="^[A-Z\xc1-\xdd]{1}[A-Za-z\xc1-\xff-'\s]{2,24}$"/>
            <br/>
            <label>Nom *</label>
            <br/>
            <input name="name" type="text" value="" required="required" pattern="^[A-Z\xc1-\xdd]{1}[A-Za-z\xc1-\xff-'\s]{2,24}$"/>
            <br/>
            <label>E-mail *</label>
            <br/>
            <input name="email" type="email" value="" required="required" pattern="^[a-z0-9._-]{3,50}@[a-z0-9.-]{3,50}\.[a-z]{2,4}$"/>
            <br/>
            <label>T&#xE9;l&#xE9;phone</label>
            <br/>
            <input name="tel" type="tel" value="" pattern="^0+[1-9]{1}((\.|-|\s)?[0-9]{2}){4}$"/>
            <br/>
            <label>Sujet</label>
            <br/>
            <input name="subject" type="text" value="" pattern="^[A-Z\xc1-\xdd]{1}[A-Za-z\xc1-\xff-'\s]{2,49}$"/>
            <br/>
            <label>Votre message *</label>
            <br/>
            <textarea name="message" maxlength="750" required="required"></textarea>
            <br/>
            <input name="contact" type="submit" value="Envoyer"/>
            <br/>
            <input name="token" type="hidden" value="b1d15e656b02672f3042793f59e21885aa9c90d0e0acb7612e"/>
        </form>
    </section>
    
    <footer>
            <a href="contact.html">
                <p>Informations légales</p>
            </a>
            <a href="contact.html">
                <p>Politique de gestion des données</p>
            </a>
            <a href="contact.html">
                <p>Formulaire de contact</p>
            </a>
   
</body>