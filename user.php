<!DOCTYPE html>
<html lang="en">
<?php session_start()?>
<head>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User</title>
    <link type="text/css" rel="stylesheet" href="./css/style.css">
</head>

<body>
    <header>
        <navbar class="container">
            <a href="indexmembres.php">
                <div class="btn_menu">Accueil</div>
            </a>
            <a href="presentation.html">
                <div class="btn_menu">Présentation du service</div>
            </a>
            <a href="produit.html">
                <div class="btn_menu">Liste des produits</div>
            </a>
            <a href="back/deconnexion.php">
                <div class="btn_menu">Déconnexion</div>
            </a>
            <a href="user.php">
                <div class="btn_menu">Utilisateur : <?php echo $_SESSION['id']?></div>
            </a>
        </navbar>

        <a href="back/deconnexion.php" ><input type="button" value="Déconnexion"></a>
        <a href="profile.php" ><input type="button" value="Profile"></a>

        <footer>
            <a href="contact.html">
                <p>Informations légales</p>
            </a>
            <a href="contact.html">
                <p>Politique de gestion des données</p>
            </a>
            <a href="contact.html">
                <p>Formulaire de contact</p>
            </a>
        </footer>
    </header>

</body>
   
